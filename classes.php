<?php
/*
 * File: classes.php
 * Project: mil-cd_studio
 * File Created: Tuesday, 29th January 2019 8:11:32 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */
?>
<?php
class INFO_CDP
{
    private $info;
    private $file_format;

    public function __construct()
    {
        $info = array(
            "ID" => "",
            "unknown_1" => "",
            "lang" => "",
            "SUB_LSN" => 0,
            "SUB_size" => 0,
        );
    }

    public static function fromFile($file)
    {
        $instance = new self();
        $instance->loadFile($file);
        return $instance;
    }

    public function loadFile($file)
    {
        $file_format =
        'A28ID/' . //ID "CD_PLUS 0100" (space padded)
        'H40unknown_1/' . //    01 00 01 00 01 or less 00 UNKNOWN 20 bytes but h/H operates on nibbles
        'a2lang/' . //Language code (ASCII 2 lower case letters)
        'NSUB_LSN/' . //Relative LSN of SUB_INFO. * File (usually 76)
        'NSUB_size'; // SUB_INFO. * File size
        //'1990xpadding'; //Just padding
        if (!$fp = fopen($file, 'rb')) {
            return 0;
        }

        if (!$data = fread($fp, 2048)) {
            return 0;
        }

        $this->info = unpack($file_format, $data);
    }

    public function printInfo()
    {
        print_r($this->info);
    }

    public function getID()
    {
        return $this->info['ID'];
    }
    public function getUnknown_1()
    {
        return $this->info['unknown_1'];
    }

    public function getLanguage()
    {
        return $this->info['lang'];
    }

    public function getSUBLSN()
    {
        return $this->info['SUB_LSN'];
    }

    public function getSUBSize()
    {
        return $this->info['SUB_size'];
    }

    public function setID($id)
    {
        $this->info['ID'] = $id;
    }
    public function setUnknown_1($unk_1)
    {
        $this->info['unknown_1'] = $unk_1;
    }

    public function setLanguage($lang)
    {
        $this->info['lang'] = $lang;
    }

    public function setSUBLSN($lsn)
    {
        $this->info['SUB_LSN'] = $lsn;
    }

    public function setSUBSize($size)
    {
        $this->info['SUB_size'] = $size;
    }
}

class SUBINFO
{
    private $header;
    private $records;
    private $mirror;

    //array of objects
    /*
    SUB_Struct
    header [ ID: "", unk1: "", records: length of records below]
    records ->
    record[ type: 1, data: "", object: [***** Actual data] ]
    record[ type: 1, data: "", object: [***** Actual data] ]
    record[ type: 1, data: "", object: [***** Actual data] ]
    etc...
    start at head, iterate down and then store the data sequentially in the file
     */

    public function __construct()
    {
        $this->header = array(
            "ID" => "",
            "unknown_1" => "",
            "records" => 0,
        );
        $this->records = array();
    }

    public static function fromFile($file)
    {
        $instance = new self();
        $instance->loadFile($file);
        return $instance;
    }

    public function setupDefaultHeader()
    {
        $this->header['ID'] = "SUB_INFO0100";
        $this->header['unknown_1'] = "0000000000000000000000000000000000000000000000000000000000000000";
        $this->header['records'] = 0;
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function getRecords()
    {
        return $this->records;
    }

    public function loadFile($file)
    {
        $header_format =
        'A12ID/' . //ID "SUB_INFO 0100"
        'H64unknown_1/' . //Unknown 32 bytes but h/H is nibbles
        'nrecords'; //Total number of records

        $record_start =
        'Ctype/' . //Record Type
        'Csize'; //number of bytes

        if (!$fp = fopen($file, 'rb')) {
            return 0;
        }
        if (!$this->mirror = fopen("MIRROR.EN", 'wb')) {
            return 0;
        }

        if (!$data = fread($fp, 46)) {
            return 0;
        }

        fwrite($this->mirror, $data);

        $this->header = unpack($header_format, $data);

        print("Found " . $this->header['records'] . " Records<br><br>\n");

        while ($data = fread($fp, 2)) {
            $record = unpack($record_start, $data);
            $padded_size = ($record['size'] + ($record['size'] % 2 == 0 ? 0 : 1));
            $record_data = fread($fp, $padded_size);
            switch ($record['type']) {
                case 1:
                    $this->parseTrack($record_data, $padded_size);
                    break;
                case 2:
                    $this->parseDiscName($record_data, $padded_size);
                    break;
                case 6:
                    $this->parseTrackName($record_data, $padded_size);
                    break;
                case 8:
                    $this->parseSingerName($record_data, $padded_size);
                    break;
                case 12:
                    $this->parseDateCode($record_data, $padded_size);
                    break;
                case 13:
                    $this->parseDateCode($record_data, $padded_size);
                    break;
                case 14:
                    $this->parseProducer($record_data, $padded_size);
                    break;
                case 15:
                    $this->parseIRSC($record_data, $padded_size);
                    break;
                case 48:
                    $this->parseJacketInfo($record_data, $data);
                    break;
                default:
                    print("Unknown Record! Type[" . $record['type'] . "], size:" . $record['size'] . " !!!<br>\n");
            }
        }
    }
    public function parseTrack($data, $length)
    {
        $track = unpack('a2', $data);
        print("<br>\nTrack:" . $track[1] . "<br>\n"); //Starts from 1?
        $this->addSection_Track($track[1]);
    }

    private function parseDiscName($data, $length)
    {
        $disc_name = unpack('a*', $data);
        $name = mb_convert_encoding($disc_name[1], "UTF-8", "SJIS");
        print("<br>\nDisc Name:" . $name . "<br>\n"); //Starts from 1?
        $this->addSection_DiscName($name);
    }
    private function parseTrackName($data, $length)
    {
        $track_name = unpack('a*', $data);
        $name = mb_convert_encoding($track_name[1], "UTF-8", "SJIS");
        print("<br>\nTrack Name:" . $name . "<br>\n"); //Starts from 1?
        $this->addSection_TrackName($name);
    }
    private function parseSingerName($data, $length)
    {
        $signer_name = unpack('a*', $data);
        $name = mb_convert_encoding($signer_name[1], "UTF-8", "SJIS");
        print("<br>\nSinger Name:" . $name . "<br>\n"); //Starts from 1?
        $this->addSection_SingerName($name);
    }
    private function parseDateCode($data, $length)
    {
        $date_data = unpack('a*', $data);
        $date = mb_convert_encoding($date_data[1], "UTF-8", "SJIS");
        print("<br>\nDateCode:" . $date . "<br>\n"); //Starts from 1?
        $this->addSection_DateCode($date);
    }
    private function parseProducer($data, $length)
    {
        $producer_name = unpack('a*', $data);
        $producer = mb_convert_encoding($producer_name[1], "UTF-8", "SJIS");
        print("<br>\nProducer:" . $producer . "<br>\n"); //Starts from 1?
        $this->addSection_Producer($producer);
    }
    //International Standard Recording Code
    private function parseIRSC($data, $length)
    {
        $isrc = unpack('a15', $data);
        print("<br>\nISRC:" . $isrc[1] . "<br>\n"); //Starts from 1?
        $this->addSection_IRSC($isrc[1]);
    }
    private function parseJacketInfo($data, $length)
    {
        $jacket_array = array();
        $jacket_format =
        'Ctype/' . //File Type is JPEG[ 0: .00 J], MPEG[ 1: .00 N, 2: .00 T, 3: .00 S ]
        'Cunknown_1/' . //Unknown
        'NLSN/' . //LSN of jacket file
        'Nsize'; //Size of Jacket File
        for ($i = 0; $i <= 3; $i++) {
            array_push($jacket_array, unpack($jacket_format, $data));
            $data = substr($data, 10);
        }
        for ($i = 0; $i <= 3; $i++) {
            print("<br>\nJacket: File Type:" . ($jacket_array[$i]['type'] == 0 ? 'JPEG' : 'MPEG') .
                "<br>&nbsp; LSN of file: " . $jacket_array[$i]['LSN'] . " with size " . $jacket_array[$i]['size'] . " bytes<br>\n"); //Starts from 1?
        }
        $this->addSection_JacketInfo($jacket_array);
    }

    public function addSection_Track($data)
    {
        $raw_data = pack("CCa*", 1, 2, $data);
        $section = array(
            "type" => 1,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);

    }

    public function addSection_DiscName($data)
    {
        $raw_data = pack("CCa*", 2, strlen($data), $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 2,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_TrackName($data)
    {
        $raw_data = pack("CCa*", 6, strlen($data), $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 6,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_SingerName($data)
    {
        $raw_data = pack("CCa*", 8, strlen($data), $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 8,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_DateCode($data)
    {
        $raw_data = pack("CCa*", 12, strlen($data), $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 12,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_Producer($data)
    {
        $raw_data = pack("CCa*", 14, strlen($data), $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 14,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_IRSC($data)
    {
        $raw_data = pack("CCa15", 15, 15, $data);
        if (strlen($data) % 2 == 1) {
            $raw_data = $raw_data . pack("C", 0x0);
        }
        $section = array(
            "type" => 15,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, $raw_data);
    }
    public function addSection_JacketInfo($data)
    {
        $raw_data = pack("CC", 48, 40);
        $section = array(
            "type" => 48,
            "data" => $data,
            "raw_data" => $raw_data,
        );
        print_r($section);
        array_push($this->records, $section);
        $num = fwrite($this->mirror, pack("H*", "3028004A00000D3300000ADF04FF000000620000180005FF0000004D0000380006FF0000005400007000")); //$raw_data);
    }

    public function printInfo()
    {
        print_r($this->header);
    }

}
