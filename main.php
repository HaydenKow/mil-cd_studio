<?php
/*
 * File: main.php
 * Project: mil-cd_studio
 * File Created: Tuesday, 29th January 2019 8:10:02 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */
?>
<?php
require "classes.php";
$info = INFO_CDP::fromFile( "INFO.CDP" );
print "
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<title>Mil-CD Studio</title>
</head>
<body>

<h1>Mil-CD Studio</h2>
<h2>INFO.CDP Parsing</h2>
<form action=\"/action.php\" name=\"info_form\" id=\"info_form\">
  File ID:<br> <input type=\"text\" name=\"ID\" disabled value=\"".$info->getID()."\"><br>
  Unknown 1:<br> <input type=\"text\" name=\"unk_1\" disabled value=\"".$info->getUnknown_1()."\"><br>
  Language:<br>
  <select name=\"lang\" form=\"info_form\">
    <option value=\"en\" ".($info->getLanguage() == "en" ? 'selected' : '') . ">English</option>
    <option value=\"ja\" ".($info->getLanguage() == "ja" ? 'selected' : '') . ">Japanese</option>
  </select><br>
  LSN of SUB_INFO: <br> <input type=\"text\" name=\"sub_lsn\" value=\"".$info->getSUBLSN()."\"><br>
  SUB_INFO size in bytes: <br> <input type=\"text\" name=\"sub_size\" value=\"".$info->getSUBSize()."\"><br>
    <br>
  <!--<input type=\"submit\" value=\"Submit\">-->
</form> 
<h2>SUB_INFO.".strtoupper($info->getLanguage())." Parsing</h2>
</body>
</html>
";
$sub = SUBINFO::fromFile("SUB_INFO.EN");

/*$sub = new SUBINFO();
$sub->setupDefaultHeader();
//print_r($sub->getHeader());

$sub->addSection_Track("00");
echo "<br>\n";
print_r($sub->getRecords());*/

?>